import {render} from '@testing-library/react';

import About from './About';

test('renders About page', () => {
  const dom = render(<About />);
  const container = dom.container;

  expect(container).not.toBeNull();
});
