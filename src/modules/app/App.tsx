import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import About from '../about/About';
import Footer from '../footer/Footer';
import Header from '../header/Header';
import Home from '../home/Home';
import Projects from '../projects/Projects';

import './App.sass';

/**
 * Main app component.
 *
 * @constructor
 */
export default function App() {
  return (
    <div className="App">
      <Router>
        <Header />

        <Switch>
          <Route path="/about">
            <About />
          </Route>

          <Route path="/projects">
            <Projects />
          </Route>

          <Route path="/">
            <Home />
          </Route>
        </Switch>

        <Footer />
      </Router>
    </div>
  );
}
