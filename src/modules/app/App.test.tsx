import {render} from '@testing-library/react';
import React from 'react';

import App from './App';

test('renders App page', () => {
  const dom = render(<App />);
  const container: Element = dom.container;

  expect(container.querySelector('header')).not.toBeNull();
  expect(container.querySelector('footer')).not.toBeNull();
});
