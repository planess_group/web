import {render} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';

import Header from './Header';

test('renders Header fragment', () => {
  const dom = render(
    <BrowserRouter>
      <Header />
    </BrowserRouter>
  );
  const container = dom.container;

  expect(container).not.toBeNull();
});
