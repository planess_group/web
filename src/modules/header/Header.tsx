import React from 'react';
import {NavLink} from 'react-router-dom';

import './Header.sass';

/**
 * Header fragment on each page.
 *
 * @constructor
 */
export default function Header() {
  return (
    <header className="general">
      <NavLink to="/">Home</NavLink>
      <NavLink to="/about">About</NavLink>
      <NavLink to="/projects">Projects</NavLink>
    </header>
  );
}
