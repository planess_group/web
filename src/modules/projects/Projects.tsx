import {Switch, Route, Link, useRouteMatch} from 'react-router-dom';

import LifeRoad from './modules/liferoad/LifeRoad';

/**
 * Projects page.
 *
 * @constructor
 */
export default function Projects() {
  const {path, url} = useRouteMatch();

  return (
    <div>
      <Switch>
        <Route exact path={path}>
          All projects

          <Link to={`${url}/liferoad`}>Life road</Link>
        </Route>

        <Route path={`${path}/liferoad`}>
          <LifeRoad />
        </Route>
      </Switch>
    </div>
  );
}
