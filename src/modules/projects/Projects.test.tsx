jest.mock('react-router-dom', () => {
  const am = jest.requireActual('react-router-dom');

  return {
    ...am,
    useRouteMatch: () => ({
      path: '/projects',
      url: '/projects'
    })
  };
});

import {render} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';

import Projects from './Projects';

test('renders Projects page', () => {
  const dom = render(
    <BrowserRouter>
      <Projects />
    </BrowserRouter>
  );
  const container = dom.container;

  expect(container).not.toBeNull();
});
