import {Link, useRouteMatch} from 'react-router-dom';

/**
 * Certain project: Life Road.
 * Main page.
 *
 * @constructor
 */
export default function LifeRoad() {
  const {url} = useRouteMatch();

  let backUrl = '/';

  const a = url.split('/');
  a.pop();

  backUrl = a.join('/');

  return (
    <div>
      <Link to={backUrl}>Back</Link>
      Life Road page
    </div>
  );
}
