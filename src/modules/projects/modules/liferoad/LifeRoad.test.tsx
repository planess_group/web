jest.mock('react-router-dom', () => {
  const am = jest.requireActual('react-router-dom');

  return {
    ...am,
    useRouteMatch: () => ({
      url: '/personal/liferoad'
    })
  };
});

import {render} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';

import LifeRoad from './LifeRoad';

test('renders Life Road project page', () => {
  const dom = render(
    <BrowserRouter>
      <LifeRoad />
    </BrowserRouter>
  );
  const container = dom.container;

  expect(container).not.toBeNull();
});
