import {render} from '@testing-library/react';

import Home from './Home';

test('renders Home page', () => {
  const dom = render(<Home />);
  const container = dom.container;

  expect(container).not.toBeNull();
});
