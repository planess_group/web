import './Footer.sass';

/**
 * Footer fragment.
 *
 * @constructor
 */
export default function Footer() {
  return <footer className="general">footer</footer>;
}
