import {render} from '@testing-library/react';

import Footer from './Footer';

test('renders Footer fragment', () => {
  const dom = render(<Footer />);
  const container = dom.container;

  expect(container).not.toBeNull();
});
